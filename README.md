# EMG_UDP_Receiver
Программа с данной QT-адаптированной библиотекой запускается после запуска программы Хоружко для 8-миканальника ech_monitor.
ech_monitor запустить с config.txt и script.qs из репозиторяи

Для компиляции в .pro файле использовать: 
QT += network widgets

Пример использования библиотеки

        REC=new EMG_UDP_Receiver();
        connect(REC,SIGNAL(sig_out(vector<float>)),this,SLOT(getEMG(vector<float>)));
		
		
